from tkinter import ttk
import tkinter as tk
from tkinter import *

ch = tk.Tk()
ch.title("DATABASE")
ch.resizable(0, 0)
ch.state("zoomed")

tbl_frame = Frame(ch)
tbl_frame.pack()

my_database = ttk.Treeview(tbl_frame, height=50)

my_database['columns'] = ('ID', 'FULL_NAME',
                          'GENDER', 'JOBTITLE', 'STATUS')

my_database.column("#0", width=0,  stretch=NO)
my_database.column("ID", anchor=CENTER, width=100)
my_database.column("FULL_NAME", anchor=CENTER, width=300)
my_database.column("GENDER", anchor=CENTER, width=400)
my_database.column("JOBTITLE", anchor=CENTER, width=400)
my_database.column("STATUS", anchor=CENTER, width=400)

my_database.heading("#0", text="", anchor=CENTER)
my_database.heading("ID", text="ID", anchor=CENTER)
my_database.heading("FULL_NAME", text="FULL_NAME", anchor=CENTER)
my_database.heading("GENDER", text="GENDER", anchor=CENTER)
my_database.heading("JOBTITLE", text="JOBTITLE", anchor=CENTER)
my_database.heading("STATUS", text="STATUS", anchor=CENTER)

my_database.insert(parent='', index='end', iid=0, text='',
                   values=('1', 'Tieng Chamrern', 'Male', 'Student', 'Single'))
my_database.insert(parent='', index='end', iid=1, text='',
                   values=('2', 'Horm Chhayheng', 'Male', 'Student', 'Single'))
my_database.insert(parent='', index='end', iid=2, text='',
                   values=('3', 'Kim Vanna', 'Male', 'Teacher', 'Married'))
my_database.insert(parent='', index='end', iid=3, text='',
                   values=('4', 'Sy Sovanna', 'Female', 'Teacher', 'Married'))
my_database.insert(parent='', index='end', iid=4, text='',
                   values=('5', 'Hor Sothy', 'Male', 'Student', 'Single'))
my_database.insert(parent='', index='end', iid=5, text='',
                   values=('6', 'Un Mekha', 'Male', 'Student', 'Married'))
my_database.insert(parent='', index='end', iid=6, text='',
                   values=('7', 'Sy Sovannarak', 'Male', 'Teacher', 'Married'))
my_database.insert(parent='', index='end', iid=7, text='',
                   values=('8', 'Kong Vothy', 'Male', 'Student', 'Single'))
my_database.insert(parent='', index='end', iid=8, text='',
                   values=('9', 'Tav Bopha', 'Female', 'Student', 'Single'))
my_database.insert(parent='', index='end', iid=9, text='',
                   values=('10', 'Kan Kanha', 'Female', 'Student', 'Single'))

my_database.pack()

ch.mainloop()
