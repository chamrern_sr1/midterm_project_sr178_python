from tkinter import *
import tkinter as tk
from tkinter import messagebox

ch = tk.Tk()
ch.title('Verify Account')
ch.geometry('400x250')
ch.config(bg='#F2BD1D')

username_var = StringVar()
password_var = StringVar()


def verify():
    username = username_var.get()
    password = password_var.get()

    if(username == "" and password == ""):
        messagebox.showwarning("empty", "Field can\'t be empty")
    elif (username == "username" and password == "password"):
        messagebox.showinfo("success", "Login Successfully")
    else:
        messagebox.showerror("invalited", "Incorrect Username and Password")


frame = Frame(
    ch,
    padx=10,
    pady=10,
    bg='#F2BD1D'
)
frame.pack(pady=0)

Label(
    frame,
    text='Verify Account',
    font=('sans-serif', 20, 'bold'),
    bg='#F2BD1D',
).grid(row=0, column=0, columnspan=5, padx=0, pady=20)

# Label_Username

Label(
    frame,
    text='Username',
    font=('sans-serif', 12),
    bg='#F2BD1D',
).grid(row=2, column=0, padx=0, pady=0)

Entry(
    frame,
    textvariable=username_var,
    validate='focusout',
    font=('sans-serif', 12)
).grid(row=2, column=1, padx=10)

# Label_Password

Label(
    frame,
    text='Password',
    font=('sans-serif', 12),
    bg='#F2BD1D'
).grid(row=3, column=0, padx=10, pady=20)

Entry(
    frame,
    textvariable=password_var,
    show='*',
    font=('sans-serif', 12)
).grid(row=3, column=1, padx=0, pady=0)

# btn_Verify

Button(
    frame,
    text='Verify',
    command=verify,
    height=0,
    width=25

).grid(row=4, column=1, sticky=E, padx=10, pady=0)

# btn_Cancel
Button(
    frame,
    text='Cancel',
    height=0,
    width=10,
    command=lambda: ch.quit()

).grid(row=4, column=0, sticky=E, padx=10, pady=0)

ch.mainloop()
